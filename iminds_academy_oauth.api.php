<?php

/**
 * Implements hook_oauth_list().
 *
 * Give a list of all OAuth types this module supports.
 */
function hook_oauth_list() {
  return array("myOwnType");
}

/**
 * Implements hook_oauth_service().
 *
 * Asks the client modules for creating an object of OAuthTools.
 */
function hook_oauth_service($id, $account) {
  if ($account["type"] == "myOwnType") {
    return new MyOwnOauthImplementation($id, $account);
  }
}

/**
 * Implements hook_oauth_test().
 *
 * Asks the module for testing requirements.
 */
function hook_oauth_test() {
  return array(
    array(
      "module" => "someModule",
      "file" => "somefile.test.inc",
      "test" => "someFunction",
      "form" => array(
        'first_test_account_email' => array(
          '#type' => 'textfield',
          '#title' => t('Account email'),
          '#description' => t('Enter the service account emailadress'),
        ),
        'first_test_private_key' => array(
          '#type' => 'managed_file',
          '#title' => t('Private key'),
          '#description' => t('Add the private key file'),
          '#upload_validators' => array(
            'file_validate_extensions' => array("p12"),
          ),
        ),
      ),
    ),
  );
}

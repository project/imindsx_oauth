<?php

function iminds_academy_oauth_list_providers() {
  $header = array("Name", "Type", "Operations");
  $rows = array();
  foreach (OAuthTools::GET() as $provider) {
    $rows[] = array(
      $provider->getDisplayName(),
      $provider->getType(),
      l(t("Edit"), "admin/config/services/oauth_provider/edit/" . $provider->getID()) . " " .
      l(t("Delete"), "admin/config/services/oauth_provider/delete/" . $provider->getID()),
    );
  }

  return array(
    "table" => array(
      "#markup" => theme('table', array("header" => $header, "rows" => $rows)),
    ),
    "operations" => array(
      "#markup" => l(t("Add provider"), "admin/config/services/oauth_provider/add"),
    ),
  );
}

function iminds_academy_oauth_add_provider_form($form, &$form_state) {
  // Create a list of all types.
  $types = module_invoke_all("oauth_list");
  foreach ($types as &$type) {
    $type = OAuthTools::CREATE(array(
      "type" => $type,
    ));
  }
  unset($type);

  // Check if oauth_type has already been chosen.
  if (!isset($form_state["oauth_type"])) {

    $form = array();
    $form['#tree'] = TRUE;

    $options = array();
    foreach ($types as $type) {
      $options[$type->getType()] = $type->getHumanType();
    }

    $form['oauth_type'] = array(
      '#type' => 'select',
      '#title' => t('Select OAuth type'),
      '#options' => $options,
      '#description' => t('Select type of OAuth provider you want to add'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Next',
    );

    return $form;
  }
  else {

    // Show settings form of type.
    foreach ($types as $type) {
      if ($type->getType() == $form_state["oauth_type"]) {
        return $type->getSettingsForm($form, $form_state);
      }
    }
  }
}

function iminds_academy_oauth_add_provider_form_submit($form, &$form_state) {

  // Create a list of all types.
  $types = module_invoke_all("oauth_list");
  foreach ($types as &$type) {
    $type = OAuthTools::CREATE(array(
      "type" => $type,
    ));
  }
  unset($type);

  // Check if oauth_type has already been chosen.
  if (!isset($form_state["oauth_type"])) {

    // Save the type.
    $form_state["rebuild"] = TRUE;
    $form_state["oauth_type"] = $form_state["values"]["oauth_type"];

  }
  else {

    // Save settings form of type.
    foreach ($types as $type) {
      if ($type->getType() == $form_state["oauth_type"]) {
        $type->submitSettingsForm($form, $form_state);
        $type->save();
        $form_state["redirect"] = "admin/config/services/oauth_provider";
        return;
      }
    }
  }
}

function iminds_academy_oauth_edit_provider_form($form, &$form_state, $pid) {
  return OAuthTools::GET($pid)->getSettingsForm($form, $form_state);
}

function iminds_academy_oauth_edit_provider_form_submit($form, &$form_state) {
  $pid = $form_state['build_info']['args'][0];
  $tool = OAuthTools::GET($pid);
  $tool->submitSettingsForm($form, $form_state);
  $tool->save();
  $form_state["redirect"] = "admin/config/services/oauth_provider";
}


function iminds_academy_oauth_delete_provider($pid) {
  OAuthTools::DELETE($pid);
  drupal_goto("admin/config/services/oauth_provider");
}

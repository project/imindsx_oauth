<?php

define('LIB_DBX_DEBUG', FALSE);

/**
 * Create Class for OAuthTools.
 */
class OAuthToolsDropbox extends OAuthTools {

  static public $APP = "iMinds Academy OAuth";
  static public $KEY = "xux633wkpcmoxl7";
  static public $SECRET = "37un6ejyba9rdlh";

  public function __construct($id, $settings) {
    parent::__construct($id, $settings);
    self::loadLib();
  }


  public function storage() {
    return new OAuthStorageDropbox($this);
  }

  public function getHumanType() {
    return t("Dropbox");
  }

  public function getSettingsForm($form, &$form_state) {

    // Create form.
    $form = array();
    $form['#tree'] = TRUE;

    $form['account_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Account name'),
      '#description' => t('Used as name for this provider'),
    );

    if (isset($this->settings['account_name'])) {
      $form['account_name']["#default_value"] = $this->settings["account_name"];
    }

    $form['folder'] = array(
      '#type' => 'textfield',
      '#title' => t('Account subfolder'),
      '#description' => t('Enter a default folder within the Google Service Account'),
    );

    if (isset($this->settings['storage_prefix'])) {
      $form['folder']['#default_value'] = $this->settings["storage_prefix"];
    }
    else {
      $form['folder']['#default_value'] = $_SERVER['SERVER_NAME'];
    }

    // Start dropbox wizard.
    $app_info = new Dropbox\AppInfo(OAuthToolsDropbox::$KEY, OAuthToolsDropbox::$SECRET);
    $web_auth = new Dropbox\WebAuthNoRedirect($app_info, OAuthToolsDropbox::$APP);

    $form['authorization'] = array(
      '#type' => 'textfield',
      '#title' => t('Dropbox authorization code'),
      '#description' => t('For connecting to the Dropbox account, we need an authorization code from Dropbox. You can obtain this by clicking following button and go through the wizard.'),
    );
    $form['get_dropbox_link'] = array(
      '#type' => 'submit',
      '#value' => t('Get authorization code'),
      '#attributes' => array(
        'onclick' => "window.open('" . $web_auth->start() . "', '_dropbox');return false;",
      ),
    );

    if (isset($this->settings['authorization'])) {
      $form['authorization']['#default_value'] = $this->settings["authorization"];
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save configuration',
    );

    return $form;
  }

  public function submitSettingsForm($form, $form_state) {

    // Defaults.
    if (!isset($this->settings["storage_prefix"])) {
      $this->settings["storage_prefix"] = "";
    }

    $old = $this->settings;

    // Add settings.
    $this->settings['account_name'] = $form_state["values"]["account_name"];
    $this->settings['storage_prefix'] = $form_state["values"]["folder"];

    // Get access token from Dropbox.
    if (!isset($this->settings['authorization']) || $this->settings['authorization'] != $form_state["values"]["authorization"]) {
      $this->settings['authorization'] = $form_state["values"]["authorization"];

      $app_info = new Dropbox\AppInfo(OAuthToolsDropbox::$KEY, OAuthToolsDropbox::$SECRET);
      $web_auth = new Dropbox\WebAuthNoRedirect($app_info, OAuthToolsDropbox::$APP);
      list($this->settings['access_token']) = $web_auth->finish($this->settings['authorization']);
    }

    // Create subfolder.
    $tmp_storage_prefix = $this->settings["storage_prefix"];
    $this->settings['storage_prefix'] = "";

    // Create directory if necessary.
    if ($this->storage()->listSubfolders($tmp_storage_prefix) === FALSE) {
      $part_path = explode("/", $tmp_storage_prefix);
      $full_path = "";
      foreach ($part_path as $part) {
        $full_path .= "/" . $part;
        $this->storage()->createFolder(substr($full_path, 1));
      }
    }
    $this->settings["storage_prefix"] = $tmp_storage_prefix;
  }


  /**
   * Load the necessary library.
   */
  protected static function loadLib() {
    try {
      $base = libraries_get_path('dropbox');
      require_once "{$base}/lib/Dropbox/autoload.php";
    }
    catch (Exception $e) {
      print 'An error occurred: '  . $e->getMessage();
      throw $e;
    }
  }

  /**
   * Get Dropbox client object.
   */
  public  function getClient() {
    return new Dropbox\Client($this->settings["access_token"], OAuthToolsDropbox::$APP);
  }
}

function _iminds_academy_oauth_dropbox_add_admin($form, &$form_state) {
  $form_state['storage']['admins']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Create Class for OAuthStorage.
 */
class OAuthStorageDropbox extends OAuthStorage {

  /**
   * Formats a path with the base path.
   *
   * Eg: '/' -> <base>, 'school' -> <base>/school
   */
  private function formatPath($path) {
    $base = $this->getDefaultFolder();

    // If a single string, format to /<path>
    if ($path[0] !== '/') {
      $path = '/' . $path;
    }

    // No base set, no need to prefix with the default folder
    // If we want the base listing (<path>'/'), remove the ending '/'
    if ($base) {
      return '/' . $base . (($path === '/') ? '' : $path);
    }
    else {
      return $path;
    }
  }

  /**
   * Set default user folder.
   */
  public function setDefaultFolder($path) {
    $this->account->settings['storage_prefix'] = $path;
  }

  /**
   * Get default user folder.
   */
  public function getDefaultFolder() {
    return $this->account->settings['storage_prefix'];
  }

  /**
   * Upload the file to google drive.
   *
   * @param string $file_path
   *   Folder/file name.
   * @param string $local_file
   *   File location/file content.
   *
   * @return bool
   *   Status of the creation.
   */
  public function upload($file_path, $local_file) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    $formatted_path = $this->formatPath($file_path);

    try {
      $folder_exists = !!$dropbox->getMetadata(dirname($formatted_path));

      if (!$folder_exists) {
        return FALSE;
      }

      // Use WriteMode::force() instead of add(), this will overwrite the file no matter what.
      $contents = is_file($local_file) ? fopen($local_file, "rb") : fopen('data://text/plain,' . $local_file, 'r');
      $dropbox->uploadFile($formatted_path, Dropbox\WriteMode::force(), $contents);

      return TRUE;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'upload: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * List folder content.
   *
   * @param string $path
   *   Rile/folder name.
   *
   * @return array
   *   Rolder object if successful, null otherwise.
   */
  public function listFiles($path = '/') {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();

    $dropbox = $this->account->getClient();
    $files = array();

    try {
      $meta = $dropbox->getMetadataWithChildren($this->formatPath($path));

      if (!$meta) {
        return FALSE;
      }

      foreach ($meta['contents'] as $content) {
        if (!$content['is_dir']) {
          // Get filename, ext included.
          $files[] = basename($content['path']);
        }
      }

      return $files;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'listSubfolders: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * List folder content.
   *
   * @param string $path
   *   Folder path of the folder for which to return the subfolders.
   *
   * @return array
   *   Array of names of subfolders if successful, empty otherwise.
   *   And false on error.
   */
  public function listSubfolders($path = '/') {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    $folders = array();

    try {
      $meta = $dropbox->getMetadataWithChildren($this->formatPath($path));

      if (!$meta) {
        return FALSE;
      }

      foreach ($meta['contents'] as $content) {
        if ($content['is_dir']) {
          // Path is the full path, we only need the foldername.
          $folders[] = basename($content['path']);
        }
      }

      return $folders;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'listSubfolders: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Create folder.
   *
   * @param string $path
   *   Path including the new folder name.
   *
   * @return bool
   *   Result of the creation.
   */
  public function createFolder($path) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();

    $formatted_path = $this->formatPath($path);

    try {
      // Make sure the parent folder exists, if no metadata is present, the folder is not present.
      $folder_exists = !!$dropbox->getMetadata(dirname($formatted_path));

      if ($folder_exists) {
        $dropbox->createFolder($formatted_path);
      }

      return $folder_exists;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'createFolder: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Set folder permission.
   *
   * @param string $path
   *   Name and path of the file.
   * @param string $email
   *   Email of the user.
   * @param string $user_role
   *   Role of the user (reader ,writer).
   *
   * @return bool
   *   return TRUE if successful , FALSE otherwise.
   */
  public function setFolderPermission($path, $email, $user_role, $notify = FALSE) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    trigger_error('Dropbox API does not support this feature.');
    return FALSE;
  }

  /**
   * Set permission.
   *
   * @param string $file_path
   *   Path and filename of the file.
   * @param string $email
   *   Email of the user.
   * @param string $user_role
   *   Role of the user (reader ,writer).
   *
   * @return bool
   *   Return TRUE if successful , FALSE otherwise.
   */
  public function setPermission($file_path, $email, $user_role, $notify = FALSE) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    trigger_error('Dropbox API does not support this feature.');
    return FALSE;
  }

  /**
   * Get user permission on the file.
   *
   * @param string $file_path
   *   Path and name of the file.
   * @param string $email
   *   Email of the user.
   *
   * @return bool
   *   Return permission object if successful , null otherwise.
   */
  public function getPermission($file_path, $email) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    trigger_error('Dropbox API does not support this feature.');
    return FALSE;
  }

  /**
   * Get user permission on the folder.
   *
   * @param string $path
   *   Name of the file.
   * @param string $email
   *   Email of the user.
   *
   * @return string
   *   return permission object if successful , null otherwise.
   */
  public function getFolderPermission($path, $email) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    trigger_error('Dropbox API does not support this feature.');
    return FALSE;
  }

  /**
   * Remove user permission on the file.
   *
   * @param string $file_path
   *   Path of the file to remove permission from.
   * @param string $email
   *   Email of the user.
   *
   * @return bool
   *   Return the delete status.
   */
  public function removePermission($file_path, $email) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    trigger_error('Dropbox API does not support this feature.');
    return FALSE;
  }

  /**
   * Remove user permission on the folder.
   *
   * @param string $path
   *   Path of the of the folder.
   * @param string $email
   *   Email of the user.
   *
   * @return bool
   *   Return the delete status.
   */
  public function removeFolderPermission($path, $email) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    trigger_error('Dropbox API does not support this feature.');
    return FALSE;
  }

  /**
   * Download a file.
   *
   * @param string $file_path
   *   Name of the file.
   *
   * @return string
   *   The file's content if successful, null otherwise.
   */
  public function download($file_path, $local_file = FALSE) {
    // Get Dropbox\Client dropbox.
    $dropbox = $this->account->getClient();
    $file = $this->formatPath($file_path);

    try {
      $link = $dropbox->createTemporaryDirectLink($file);

      if (!$link) {
        return FALSE;
      }

      // fyi: second item contains a DateTime object.
      $link = $link[0];
      $content = file_get_contents($link);

      if (!$local_file) {
        return $content;
      }

      file_put_contents($local_file, $content);
      return TRUE;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'download: ' . $e->getMessage();
      }
      return FALSE;
    }

  }

  private function delete($path) {
    try {
      $dropbox = $this->account->getClient();
      $dropbox->delete($this->formatPath($path));
      return TRUE;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'delete: ' . $e->getMessage();
      }
      return FALSE;
    }
  }
  /**
   * Delete file.
   *
   * @param string $file_path
   *   Name of the file.
   */
  public function deleteFile($file_path) {
    return $this->delete($file_path);
  }

  /**
   * Delete folder.
   *
   * @param string $path
   *   Name of the folder.
   */
  public function deleteFolder($path) {
    return $this->delete($path);
  }

  /**
   * Gets the direct download link of a file.
   *
   * @return string
   *    $link, direct link to the file
   */
  public function getDownloadLink($file_path) {
    try {
      $dropbox = $this->account->getClient();
      $temp_link = $dropbox->createTemporaryDirectLink($this->formatPath($file_path));
      // fyi, second contains a timestamp when to expire.
      return $temp_link[0];
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'getDownloadLink: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Gets a file metadata.
   *
   * @return array,
   *   Returns an array with the metadata
   *   'created' field is always null because unsupported
   */
  public function getFileMetadata($file_path) {
    try {
      $dropbox = $this->account->getClient();
      $meta = $dropbox->getMetadata($this->formatPath($file_path));

      return array(
        'created' => NULL,
        'modified' => strtotime($meta['modified']),
        'mimetype' => $meta['mime_type'],
      );
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'getFileMetadata: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Gets a folder metadata.
   *
   * @return array
   *   Returns an array with the metadata
   *   'created' field is always null because unsupported
   */
  public function getFolderMetadata($path) {
    try {
      $dropbox = $this->account->getClient();
      $meta = $dropbox->getMetadata($this->formatPath($file_path));
      return array(
        // fyi, unsupported by Dropbox.
        'created' => NULL,
        'modified' => strtotime($meta['modified']),
      );
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'getFolderMetadata: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Copies a file from path to dest.
   *
   * @return bool
   *   Returns if the file was copied successfully.
   */
  public function copyFile($file_path, $dest) {
    try {
      // note: if the file exists, dropbox will drop an error, so we do not have to double check for it.
      // eg: 403: "A file with that name already exists at path '/test/subfolder/file.txt.
      $dropbox = $this->account->getClient();
      $dropbox->copy($this->formatPath($file_path), $this->formatPath($dest));
      return TRUE;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'copyFile: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Copies a folder from path to dest.
   *
   * @return bool
   *   Returns if the folder was moved successfully.
   */
  public function copyFolder($path, $dest) {
    try {
      $dropbox = $this->account->getClient();
      $dropbox->copy($this->formatPath($path), $this->formatPath($dest));
      return TRUE;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'copyFolder: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Moves a file from path to dest.
   *
   * @return bool
   *   Return if file was moved successfully.
   */
  public function moveFile($file_path, $dest) {
    try {
      $dropbox = $this->account->getClient();
      $dropbox->move($this->formatPath($file_path), $this->formatPath($dest));
      return TRUE;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'moveFile: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

  /**
   * Moves a folder from path to dest.
   *
   * @return Bool
   *   Return if the folder has moved.
   */
  public function moveFolder($path, $dest) {
    try {
      $dropbox = $this->account->getClient();
      $dropbox->move($this->formatPath($path), $this->formatPath($dest));
      return TRUE;
    }
    catch (Exception $e) {
      if (LIB_DBX_DEBUG) {
        print 'moveFolder: ' . $e->getMessage();
      }
      return FALSE;
    }
  }

}

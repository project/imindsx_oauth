<?php

class OAuthTools {

  public static function CREATE($account) {

    // module_invoke_all invokes hook "oauth_service" in all module that implement it.
    $classes = module_invoke_all("oauth_service", FALSE, $account);
    if (count($classes) > 1) {
      trigger_error("Multiple oauth service classes were returned");
    }

    return $classes[0];
  }

  public static function USER_GET($id = FALSE) {
    // TODO.
  }

  public static function GET($id = FALSE) {
    // Get data out of the database.
    $query = db_select("iminds_academy_oauth_providers", "p")
      ->fields('p', array('pid', 'settings'));
    if ($id != FALSE) {
      $query = $query->condition("pid", $id);
    }
    $results = $query->execute()->fetchAll(PDO::FETCH_ASSOC);

    $return = array();
    foreach ($results as $row) {
      $row['settings'] = unserialize($row['settings']);

      $classes = module_invoke_all("oauth_service", $row['pid'], $row['settings']);
      if (count($classes) > 2) {
        trigger_error("Multiple oauth service classes were returned");
      }

      $return[] = $classes[0];
    }

    if ($id === FALSE) {
      return $return;
    }
    else {
      return $return[0];
    }
  }

  public static function DELETE($id) {
    db_delete("iminds_academy_oauth_providers")
      ->condition("pid", $id)
      ->execute();
    return TRUE;
  }

  public function __construct($id, $settings) {
    $this->id = $id;
    $this->settings = $settings;

    if (empty($settings['storage_prefix'])) {
      $settings['storage_prefix'] = "";
    }
  }

  public function storage() {
    return new OAuthStorage($this);
  }

  public function getType() {
    return $this->settings["type"];
  }

  public function getHumanType() {
    return $this->settings["type"];
  }

  public function getDisplayName() {
    return $this->settings["account_name"];
  }

  public function getID() {
    return $this->id;
  }

  public function save() {
    if ($this->id === FALSE) {
      $row = array(
        "settings" => $this->settings,
      );

      drupal_write_record("iminds_academy_oauth_providers", $row);
      $this->id = $row["pid"];
      return $row["pid"];

    }
    else {
      $row = array(
        "pid" => $this->id,
        "settings" => $this->settings,
      );
      drupal_write_record("iminds_academy_oauth_providers", $row, "pid");
      return TRUE;
    }
  }
}


class OAuthStorage {
  public function __construct($account) {
    $this->account = $account;
  }

  public function createFolder($name) {
  }
}

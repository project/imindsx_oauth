# iMinds Academy OAuth

A library defining a common set of API's for connecting to 3rd party applications or API's.
The API's are grouped into several classes, all having a subset of functionality.
For example the storage API will define all functionality for talking to e.g. Dropbox API and Google Drive API.
Because this gives an abstraction over all different 3rd party API's, you can quickly switch between the different providers without learning a new API.

The different classes:

- Storage
- Login (WIP)
- Calendar (WIP)
- SocialFeed (WIP)
- Tasks (WIP)

[![License](https://poser.pugx.org/learninglocker/learninglocker/license.svg)](http://opensource.org/licenses/GPL-3.0)

### API

The setup and adding of the credentials are done in the Drupal backend. You can get a list of all defined accounts by listing them.

```php
<?php
$accounts = OAuthTools::GET(); // Get the OAuth account defined by ID 1
foreach($accounts as $account) {
  var_dump($account->getDisplayName()); // Get the user defined display name of the account
  var_dump($account->getID()); // Get the internal ID of the account
}
?>
```

Once an account has been choosen, you can get the account by ID.

```php
<?php
$account = OAuthTools::GET($id);
?>
```

## Storage
Currently implemented providers: Google Service Account, Dropbox

The storage class provides an API to upload/share/move/edit files inside a cloud provider like Dropbox.
It provides the same interface for all providers, so if it works with one, it works with all providers.

An example usage of this API can be seen in iMinds Academy Storage module.
This module let's you add an upload field to a node, so the user can upload files to the cloud.

### API

Function                                | Summary
---                                     | ---
listFiles($path = '/')                  | Get all files from a path in an array.
                                        | **$path**: The path from which it should list the files. For getting files from subfolder, you can use forward slashes: 'some/subfolder/from/something'. For the root folder, you can use '/'
                                        | When the path doesn't exists, it will return FALSE.
                                        | When the folder is empty, it will return array().
listSubfolders($path = '/')             | Get all folders from a path.
                                        | **$path**: the path from which it should list the folders. For getting the folders from a subfolder, you can use forward slashes: 'some/subfolder/from/something'. For the root folder, you can use '/'
                                        | When the path doesn't exists, it will return FALSE.
                                        | When the folder is empty, it will return array().
upload($filePath, $local_file)          | This will upload a file to the specified path.
                                        | **$filePath**: The path where the file should be inserted. This path should exist.
                                        | **$local_file**: Either you provide a path to a local file or you add the file contents as string.
                                        | When the filePath doesn't exists, it will return FALSE and the file will not be uploaded
download($filePath, $local_file=false)  | This will download a files from the storage account to a specified path
                                        | **$filePath**: the path and filename from where the file has to be downloaded
                                        | **$local_file**: Either this is false and the contents of the file will be returned or you provide a path to a local file that can be created.
                                        | When the path doesn't exists, it will return FALSE
                                        | When the local_file is given, but there are no permission to create the file, it will return FALSE
createFolder($path)                     | Create the folder or subfolder
                                        | **$path**: the path that has to be created. This also includes the new folder that has to be created. For creating a subfolder, you can use forward slashes: 'create/subfolder'
                                        | When the parent folder doesn't exists, the directory will not be created and return FALSE.
deleteFile($filePath):                  | Delete the specified file
                                        | **$filePath**: the path and filename which file has to be deleted
                                        | When the file doesn't exists, it will return FALSE
deleteFolder($path)                     | Delete folder. Will remove all directories and files within it.
                                        | **$path**: the folder that has to be deleted. The path and the folder that has to be created
                                        | When the path doesn't exists or isn't a folder, it will return FALSE
copyFile($filePathFrom, $filePathTo)    | Copy the file from one destination to another.
                                        | **$filePathFrom**: The path and filename of the file that has to be copied
                                        | **$filePathTo**: The path and filename of the file to where it has to be copied
                                        | Return FALSE when the $filePathFrom doesn't exists
                                        | Return FALSE when the $filePathTo already exists
                                        | Return FALSE when the destionation path doesn't exist
copyFolder($pathFrom, $pathTo)          | Copy the folder from one destination to another. All subfiles and subfolders will also be copied.
                                        | **$pathFrom**: The folder and it's contents that has to be copied
                                        | **$pathTo**: The full destination path (including the not yet created destination folder)
                                        | Return FALSE when the $pathFrom folder doesn't exist
                                        | Return FALSE when the $pathTo folder already exists
                                        | Return FALSE when the parent directory of the $pathTo doesn't exists
moveFile($filePathFrom, $filePathTo)    | Move the file from one destination to another.
                                        | **$filePathFrom**: The path and filename of the file that has to be moved
                                        | **$filePathTo**: The path and filename of the file to where it has to be moved
                                        | Return FALSE when the $filePathFrom doesn't exists
                                        | Return FALSE when the $filePathTo already exists
                                        | Return FALSE when the destionation path doesn't exist
moveFolder($path, $path)                | Move the folder from one destination to another. All subfiles and subfolders will also be copied.
                                        | **$pathFrom**: The folder and it's contents that has to be moved
                                        | **$pathTo**: The full destination path (including the not yet created destination folder)
                                        | Return FALSE when the $pathFrom folder doesn't exist
                                        | Return FALSE when the $pathTo folder already exists
                                        | Return FALSE when the parent directory of the $pathTo doesn't exists
getDownloadLink($filePath)              | Returns a link where you can download the file.
                                        | **$filePath**: The path and filename of the file you want a download link from
                                        | Note: This will not set the download permissions. So if you want a download link that can be downloaded by everyone, you should do setPermission($path, "anyone", "read");
                                        | Note: On google drive when the document can be opened embedded, the embedded version is given.
                                        | Return FALSE when the filepath doesn't exist
setPermission($filePath, $email, $role, $notify=false) | Set permission on a file, so that external users can access the file in their provider account (Google Drive of Dropbox).
                                        | **$filePath**: The path and filename of the file that will get additional permission
                                        | **$email**: The identifier with who the file has to be shared (or who has access to the file). Most providers use emails, but this can be extended to non-email identifiers later. There is also a hard-coded identifier. If you set the email to "anyone", than the permission will be applied for everyone.
                                        | **$role**: The permission that should be applied. There is "read", "write" and "comment" permission.
                                        | Return FALSE when the filepath doesn't exists.
getPermission($filePath, $email)        | Get the permission on a file for a particular email
                                        | **$filePath**: The path and filename of the file you want to get the permission
                                        | **$email**: The identifier or person you want the permission from (Can be 'anyone')
                                        | Return FALSE when the filePath doesn't exist
                                        | Return FALSE when there is no permission set for the file/email combo
                                        | Return the permission if the permission is found. This can be "read", "write" or "comment"
removePermission($filePath, $email)     | Remove permission of a file/email combe
                                        | **$filePath**: The path and filename of the file from the permission that has to be deleted
                                        | **$email**: The identifier of person that has to be deleted (Can be 'anyone')
                                        | Return FALSE when the filePath doesn't exist
                                        | Return FALSE when there is no permission set for the file/email combo
setFolderPermissions($path, $email, $role, $notify=false) | Set permission on a folder, so that external users can access the file in their provider account (Google Drive of Dropbox).
                                        | **$file**: The path of the folder that will get additional permission
                                        | **$email**: The identifier with who the folder has to be shared (or who has access to the file). Most providers use emails, but this can be extended to non-email identifiers later. There is also a hard-coded identifier. If you set the email to "anyone", than the permission will be applied for everyone.
                                        | **$role**: The permission that should be applied. There is "read", "write" permission.
                                        | Return FALSE when the path doesn't exists.
getFolderPermission($path, $email)      | Get the permission on a folder for a particular email
                                        | **path**: The path of the folder you want to get the permission
                                        | **$email**: The identifier or person you want the permission from (Can be 'anyone')
                                        | Return FALSE when the path doesn't exist
                                        | Return FALSE when there is no permission set for the path/email combo
                                        | Return the permission if the permission is found. This can be "read" or "write"
removeFolderPermission($path, $email)   | Remove permission of a path/email combe
                                        | **$filePath**: The path of the folder from the permission that has to be deleted
                                        | **$email**: The identifier of person that has to be deleted (Can be 'anyone')
                                        | Return FALSE when the path doesn't exist
                                        | Return FALSE when there is no permission set for the path/email combo

### Example

```php
<?php
$oauth = OAuthTools::GET(1); // Get the OAuth account defined by ID 1
$storage = $oauth->storage(); // Get the storage API
$files = $storage->listFiles("/"); // List all files on the root of account
var_dump($files); // array("photo.png", "photo2.png")
?>
```

## Login
Currently implemented providers: None

The login class will provide a method of getting login information from the users.
So once the user has been authenticated, you will be able to get the email/avatar/username.

### API

Function                                | Summary
---                                     | ---
isLoggedIn()                            | Check if user has logged in
username()                              | Get the username of the user
email()                                 | Get the email of the user
avatar()                                | Get the avatar of the user (blob)
